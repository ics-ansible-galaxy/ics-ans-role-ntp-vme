ics-ans-role-ntp-vme
===================

Configure NTP daemon in VME rootfs.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

- ...

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: ics-ans-role-ntp-vme }
```

License
-------

BSD 2-clause
